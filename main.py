import os
from flask import Flask, request, make_response
from dotenv import load_dotenv
from core import Pythogram, EventListener
from core.domains import Webhook, Buttons, Button
from events import *

config_file = os.path.abspath('./.env')
if not os.path.exists(config_file) or not os.path.isfile(config_file):
    raise Exception('File .env not found')

load_dotenv(config_file)
for key in ['telegram_webhook_url', 'telegram_token']:
    value = os.environ.get(key)
    if not value:
        raise Exception(f'Setting with name "{key}" is undefined of empty')
telegram_token = os.environ.get('telegram_token')
url_path = f'/post-{telegram_token}'
events = EventListener()
app = Flask(__name__)


@app.route(url_path, methods=['GET', 'POST'])
def telega():
    if request.method == 'POST':
        message = Pythogram.parse_message(request.json)
        events.call(message=message)
    return make_response({'ok': True})


if __name__ == '__main__':
    t = Pythogram(token=os.environ.get('telegram_token'))
    t.webhook.set(Webhook(url=os.environ.get('telegram_webhook_url')+url_path))
    menu_items = {
        '/ru': 'Русский',
        '/en': 'English',
        '/fr': 'Français',
        '/de': 'Deutsch'
    }
    menu = Buttons()
    for key, value in menu_items.items():
        menu.add(Button(id=key, text=value))
    events.subscribe(ActionLanguage(sender=t, menu=menu))
    app.run(host='0.0.0.0')

