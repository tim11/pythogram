from dataclasses import dataclass
from typing import Tuple


@dataclass(frozen=True)
class Button:
    id: str
    text: str


class Buttons:
    def __init__(self, *args: Button):
        self.__buttons = []
        if args:
            self.__buttons.extend(args)

    def add(self, button: Button):
        self.__buttons.append(button)

    @property
    def items(self) -> Tuple[Button]:
        return tuple(self.__buttons)

