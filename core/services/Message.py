import json
from core.domains import Chat, Buttons, Button
from core.base import Service


class Message(Service):
    def send(self, text: str, chat: Chat, menu: Buttons = None):
        data = {
            'chat_id': chat.id,
            'text': text
        }
        if menu:
            data['reply_markup'] = json.dumps({
                'inline_keyboard': [
                    [
                        {'text': button.text, 'callback_data': button.id}
                        for button in menu.items
                    ]
                ]
            })
        response = self.http.request('/sendMessage', data=data)
